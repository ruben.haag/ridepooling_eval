function total_length(job)::Float64
    job.length
end

function driven_time(job)::Float64
    job.duration
end

