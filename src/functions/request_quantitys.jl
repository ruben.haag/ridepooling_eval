function count_req(req)
    return 1
end

function relative_delay(req)

    if req.t_dropoff > 0    #only dropped-off requests
        t_actual = req.t_dropoff - (req.t_submit + req.dt_earliest_pickup)

        time = (t_actual - req.direct_time) / req.direct_time
        return time
    end
    return nothing
end

function delay(req)
    if req.t_dropoff > 0    #only dropped-off requests

        counter = 1

        t_actual = req.t_dropoff - (req.t_submit + req.dt_earliest_pickup)

        time = (t_actual - req.direct_time)
        return time
    end
    return nothing
end


function relative_waiting_time(req)
    if req.t_pickup > 0    #only picked-up requests
        time = (req.t_pickup - (req.t_submit + req.dt_earliest_pickup)) / req.direct_time
        return time
    end
    return nothing
end

function waiting_time(req)
    if req.t_pickup > 0    #only picked-up requests
        time = req.t_pickup - (req.t_submit + req.dt_earliest_pickup)
        return time
    end
    return nothing
end

function travelling_time(req)
	if req.t_dropoff > 0
    	return req.t_dropoff - req.t_pickup
	else
		return nothing
	end
end

function total_time(req)
	if req.t_dropoff > 0
    	return req.t_dropoff - (req.t_submit + req.dt_earliest_pickup)
	else
		return nothing
	end
end

function served(req)
    if req.bus_id > 0
        return 1
    else
        return 0
    end
end




function requested_distance(req)
    if req.bus_id > 0    #only non-rejected requests

        distance = req.direct_length
        return distance
    end
    return nothing
end

function requested_time(req)
    if req.bus_id > 0    #only non-rejected requests
        time = req.direct_time
        return time
    end
    return nothing
end

function resubmitted_all(req)
	req.n_resubmitted
end

function resubmitted_served(req)
    if req.bus_id > 0    #only non-rejected requests
        return req.n_resubmitted
    end
    return nothing
end

function resubmitted_served_percentage(req)
    if req.bus_id > 0		#only non-rejected requests
		if req.n_resubmitted > 0    
        	return 1
		else
			return 0
		end
    end
    return nothing
end


function dt_earliest(req)
    return req.dt_earliest_pickup
end

function dt_earliest_served(req)
	if req.t_pickup > 0 #Only Requests that where picked up
    	return req.dt_earliest_pickup
	else
		return nothing
	end
end

# TODO find a name for this function
function relative_dt_earliest(req)
	if req.t_pickup > 0 #Only Requests that where picked up
    	return req.dt_earliest_pickup / (waiting_time(req) + req.dt_earliest_pickup)
	else
		return nothing
	end
end