
export sum, mean, variance


function sum(f, model::ABM)::Float64
    return sum(f, model.requests)
end
""" sum(f::function, request_list)
    f(req) -> c, value
    sums up all values, where c is true
"""
function sum(f, list)::Float64
    f = @eval $f
    x = 0.0
    for req in list
        value = f(req)
        if value != nothing
            x += value
        end
    end
    return x
end


function mean(f, model::ABM)::Float64
    return mean(f, model.requests)
end
""" sum(f::function, request_list)
    f(req) -> c, value
    mean of all values, where c is true
"""
function mean(f, list, args=nothing)::Float64
    f = @eval $f

    counter = 0
    x = 0.0
    for req in list
        #a ? b : c

        value = args==nothing ? f(req) : f(req, args)
        if value != nothing
            x += value
            counter += 1
        end
    end
    return x/counter
end


function variance(f, model::ABM)::Float64
    return variance(f, model.requests)
end
""" sum(f::function, request_list)
    f(req) -> c, value
    variance of all values, where c is true
"""
function variance(f, list, args=nothing)::Float64
    #Mean Distance
    MD = mean(f, list, args)
    f = @eval $f
    #Variance
    var = 0.0
    counter = 0.0
    for req in list
        value = args == nothing ? f(req) : f(req, args)
        if value != nothing
            var += (value - MD)^2
            counter += 1
        end
    end
    return sqrt(var/counter^2)
end

function quantity(f, list)::Measurement
    measurement(mean(f, list), variance(f, list))
end

function quantity(f, list, args)::Measurement
    measurement(mean(f, list, args), variance(f, list, args))
end

function values(f, list, args=nothing)
	(args == nothing) ?  (@eval $f.(list)) : (@eval $f.(list, args))
end