
function cooldown_time(agent, model::ABM)
	bus=agent[2]
	if length(bus.todo) == 0
		return 0
	end
	return sum(driven_time, bus.todo) - (model.time - bus.todo[1].t_start)
end


