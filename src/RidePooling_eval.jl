module RidePooling_eval


using Measurements # For error Propagation



using RidePooling:ABM, Job
include("functions/request_quantitys.jl")
include("functions/job_quantitys.jl")
include("functions/bus_quantitys.jl")
include("functions/statistics.jl")


function driven_distance(model::ABM)
    sum(:total_length, Iterators.flatten(model.job_history))
end
function driven_time(model::ABM)
    sum(:driven_time, Iterators.flatten(model.job_history))
end

function efficiency(model::ABM)
    sum(:requested_distance, model.requests)/driven_distance(model)
end
function quality(model::ABM)
	sum(:requested_time, model.requests)/sum(:total_time, model.requests)

end

function idle_time(model::ABM)::Float64
	total_time = model.time
	job_list = Iterators.flatten(model.job_history)

    return (total_time- sum(:driven_time, job_list)/length(model.agents) ) / total_time
end




function performance_factor(model::ABM, vanilla_model::ABM)

    t_wait = quantity(:waiting_time, model)
    t_wait_vanilla = quantity(:waiting_time, vanilla_model)

    return t_wait/t_wait_vanilla
end

function model_time(model::ABM)
    return model.time
end





end # module
